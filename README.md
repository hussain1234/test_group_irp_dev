
#services:
#- mysql

variables:
  # Configure mysql service (https://hub.docker.com/_/mysql/)
  MYSQL_DATABASE: hello_world_test
  MYSQL_ROOT_PASSWORD: mysql

#connect:
  #image: mysql
 # script:
  #- echo "SELECT 'OK' ;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"

build_job_1:
     stage: build
     script:  
       - if [ "$BUILD" != "" ];then echo $BUILD ;else exit 0;fi;
     
       - for SCRIPT_TO_RUN in `find . -name rpt -type d -o -name enterprise -type d -o -name reporting -type d -o -name registry -type d -o -name mts -type d|sed -e 's/\.\///;s/\(.*\)\/.*/\1/'`;do
       - echo "$SCRIPT_TO_RUN"
       - mkdir -p sqlplusFolder/inputDev/output        
       - mkdir -p sqlplusFolder/logs/        
       - if [[ -d "$SCRIPT_TO_RUN"  ]];then mkdir -p sqlplusFolder/inputDev/"$SCRIPT_TO_RUN";cp -r "$SCRIPT_TO_RUN"   sqlplusFolder/inputDev/"$SCRIPT_TO_RUN"/../ ; fi;    
       - if [[ -d "releases/" ]];then mkdir -p sqlplusFolder/inputDev/releases/ ;cp -r releases/  sqlplusFolder/inputDev/ ; fi;   
       - sed -e "s/<${DB_USER}_PASSWORD>/${DB_PASSWORD}/i" ${CONFIG_FILE} > sqlplusFolder/config-aws-dev.csv
       - cd sqlplusFolder/
       - cat ${config:-config-aws-dev.csv}
       - echo java -jar /opt/iqvia/DBUtility-01.jar -configFile ${config:-config-aws-dev.csv} -inputPath inputDev -inputDirs "$SCRIPT_TO_RUN"
       - sleep 3
       - cd inputDev/output/
       - if [ "$BUILD" == "utbuild" ];then for i in `find  "$SCRIPT_TO_RUN" -name schemas.sql|grep  '/rpt'`;do sed -i  "s/^connect .*@/connect ${DB_UT_USER}\/${DB_UT_PASSWORD}@/i" $i;done;fi
       - if [ "$BUILD" == "utbuild" ];then for i in `find  "$SCRIPT_TO_RUN" -name schemas.sql|grep -v '/rpt'`;do sed -i  "s/^connect .*@/connect ${DB_UT_RDC_USER}\/${DB_UT_RDC_PASSWORD}@/i" $i;done;fi
       - echo sqlplus /nolog @multiMaster.sql|sed -e 's/\(connect [^\/]*\/\)[^@]*@/\1xxxx@/i'  >> ../../logs/output.txt
       - cd ../../../
       - done
     artifacts:
      name: output.txt
      paths: 
      - sqlplusFolder/logs/

build1:
#  image: mysql
  stage: build
  script:
   - echo "build" 
   - exit 0
   - export getlastcommittime="`cat .committime`"
   - export commitfiles="`git log --after="$getlastcommittime"|sed -ne '/^[ ]*Update[ ]*/s/^[ ]*Update[ ]*\(.*\)/\1/p'|sort|uniq`"
   - export newcommittime="`git log --after=\"$getlastcommittime\"|sed -ne '/^Date:.*/s/^Date:[ ]*\(.*\)/\1/p'|head -1`"
   - echo  -e "$getlastcommittime \n $commitfiles \n $newcommittime"
   - echo $newcommittime > lastcommittime
   - git clone git@gitlab.com:hussain1234/test_group_irp_dev.git
   - git add lastcommittime
   - git commit -m'lastcommittime'
   - git push
#   - echo -e  "DELIMITER // \n CREATE PROCEDURE Getdate() \n BEGIN \n	SELECT  CURDATE();\n END //\n DELIMITER ;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE" 
#   - echo "call Getdate() ;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
 

  
test1:
 # image: mysql
  stage: test
  script:
    - echo "test"
    - whereis git
    - exit 0
    - echo "call Getdate() ;" 
    - echo "call Getdate() ;"  mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE" 2>&1 > log.txt
    - cat log.txt
  allow_failure: true