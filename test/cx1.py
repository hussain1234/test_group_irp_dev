# query.py

import cx_Oracle

# Establish the database connection
connection = cx_Oracle.connect(user="ST1_ENT1", password="b1gb0x",
                               dsn="irprdcd.cksi9gx3500i.us-east-1.rds.amazonaws.com/IRPRDCD")

# Obtain a cursor
cursor = connection.cursor()

# Data for binding
manager_id = 20
first_name = "Peter"

# Execute the query
sql = """SELECT username
         FROM account
         WHERE rownum <= :mid """
cursor.execute(sql, mid=manager_id)

# Loop over the result set
for row in cursor:
    print(row)
